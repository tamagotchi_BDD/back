const db = require('../config/Database');
const fs = require("fs");
const path = require("path");
const {parseProcedureSqlFile, parseBasicSqlFile, formatDate} = require("./helper");
const {createTransaction} = require("../config/SQLRequest");
require('dotenv').config({path: path.resolve(__dirname, '../.env')});

/**
 * Set the difficulty
 */
manageMode = async () => {
  const mapper = {
    easy : "table_easy_tamagotchi.sql",
    medium : "table_medium_tamagotchi.sql",
    hard : "table_hard_tamagotchi.sql",
  }
  const modeFile = mapper[process.env.DIFFICULTY] || "table_easy_tamagotchi.sql"
  // Pastes the right file to setup folder depending on difficulty
  await fs.copyFile(path.resolve(__dirname) + '/../mode/' + modeFile, path.resolve(__dirname) + '/../setup/table_tamagotchi.sql', (err) => {
    if (err) throw err;
    console.log('Difficulty was setup to : ' + process.env.DIFFICULTY);
  });
}

/**
 * Create the migration file
 */
exports.makeMigration = async () => {
  // Set the difficulty
  await manageMode()
  // Removes all migrations files of migration folder
  await fs.readdir(path.resolve(__dirname) + '/../migrations', (err, files) => {
    files.pop();
    files.forEach((file) => {
      fs.unlink(path.resolve(__dirname) + '/../migrations/' + file, (err) => {
        if (err) throw err;
      })
    })
  })

  await db
    .connect(async error => {
      if (error) console.log('Erreur lors de la connexion à la base de donnée. Elle n\'existe pas.')
      else {

        let request = `
            SELECT count(*) AS result
            FROM information_schema.TABLES
            WHERE table_schema = '${process.env.DB_NAME}'
              AND table_name = 'migrations'
            LIMIT 1;`

        // Check if the table migration exist
        await db.execute(request, function (err, data) {
          let hasTableExist = data[0].result
          // if no table is found
          if (hasTableExist === 0) {
            // STEP 1 : READ EVERY FILE INTO SQL FOLDER AND PUSH EVERY QUERY IN A TEMP VARIABLE
            try {
              fs.readdir(path.resolve(__dirname) + '/../setup', (err, files) => {
                if (err) {
                  console.error("Could not list the directory.", err);
                  process.exit(1);
                }
                let queries = [];
                // All queries array start by setting foreign_key_checks to 0
                // Disabled constraints on foreignKey
                queries.push([`SET foreign_key_checks = 0;`])

                // JUST IN CASE OF TABLES EXISTS IN THE SELECTED DATABASE
                queries.push(parseBasicSqlFile(path.resolve(__dirname) + '/../config/Reset.sql'));

                // Set all queries of each file into a queries array
                files.forEach((file) => {
                  // The parsing of procedure or function are not the same of basic sql file
                  if (file.match(/procedure/g) || file.match(/function/g) || file.match(/trigger/g)) {
                    queries.push(parseProcedureSqlFile(path.resolve(__dirname) + '/../setup/' + file));
                  } else {
                    queries.push(parseBasicSqlFile(path.resolve(__dirname) + '/../setup/' + file));
                  }
                })
                // At the end of queries arrays set foreign_key_checks to 1
                queries.push([`SET foreign_key_checks = 1;`])

                // Need to concat all array before join !
                // If array are not concat, join method will not work on array with several elements
                queries = queries.flat()

                // Set the date with format YYYY-MM-DD
                let dateFile = formatDate(Date.now())
                // CREATE MIGRATION FILE
                // Join all queries with a break line
                fs.appendFile(path.resolve(__dirname) + `/../migrations/migration${dateFile}.sql`, queries.join('\n'), function (err) {
                  if (err) throw err;
                  // Stop process with success
                  console.log("SUCCESS !");
                  process.exit()
                });
              })
            } catch (err) {
              console.log("Error :", err);
              process.exit(1)
            }
          } else {
            console.log("DROP YOUR DATABASE PLEASE !");
          }
        })
      }
    })
}

/**
 * Execute migration files
 */
exports.migrate = () => {
  fs.readdir(path.resolve(__dirname) + '/../migrations', (err, files) => {
    if (err) {
      console.error("Could not list the directory.", err);
      process.exit(1);
    }
    let queries = [];

    // Set all queries of each file into a queries array
    files.forEach((file) => {
      console.log("migrate :", file);
      queries.push(fs.readFileSync(path.resolve(__dirname) + '/../migrations/' + file, 'utf8'));
    })

    // Split all requests to send it one by one
    let request = []
    queries.forEach(element => {
      request.push(...element.split("\n").filter((e) => e !== ''))
    });
    try {
      createTransaction(request)
        .then(() => {
          db.query(`CALL CREATE_MIGRATION("${files.at(-1)}", "")`, function (err) {
            if (err) throw err;
            console.log("SUCCESS !");
            process.exit()
          });
        })
    } catch (err) {
      console.log("Error :", err);
      process.exit(1)
    }
  })
}
