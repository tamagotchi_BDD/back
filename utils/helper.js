const fs = require("fs");

exports.parseBasicSqlFile = (url) => {
  let query = fs.readFileSync(url).toString();

  query = query.split(";")
    .map((e) => {
      e = e
        .replaceAll('\r', '')
        .replaceAll('\n', '')
        .replaceAll('\t', '')
      return e;
    })
    .filter((e) => e !== '');

  return query;
}

exports.parseProcedureSqlFile = (url) => {
  let procedure = fs.readFileSync(url).toString();

  procedure = procedure
    .replaceAll("\n", "")
    .replace(/DELIMITER ;?;/gm, '')
    .replace(/;;/gm, ';')

  return procedure;
}

exports.formatDate = (date, joinSymbol="", format="default") => {
  let d = new Date(date),
  month = '' + (d.getMonth() + 1),
  day = '' + d.getDate(),
  year = d.getFullYear(),
  hour = d.getHours(),
  minute = d.getMinutes()

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  let dateF = [year, month, day].join(joinSymbol)
  if(format === "long") dateF = dateF + '-' + [hour, minute].join(":")

  return dateF;
}
