const db = require('../config/Database');
const {formatDate} = require('../utils/helper')

/**
 * CREATE tamagotchi
 * @param {*} req
 * @param {*} res
 */
exports.create = (req, res) => {

  let params = req.body

  // Check if params are not empty
  if (!params.id || isNaN(params.id) || !params.tamagotchiName) {
    res.status(500).json({
      success: false,
      error: "'id' (Number) or 'tamagotchiName' (String) are empty"
    });
    throw formatDate(Date.now(), "-", "long") + " 'id' (Number) or 'tamagotchiName' (String) are empty";
  }
  // Create tamagotchi with call on procedure
  try {
    // Parsing string to int into try section just in case to catch error
    if (typeof params.id == "string") params.id = parseInt(params.id)

    db.query(`CALL CREATE_TAMAGOTCHI("${params.id}", "${params.tamagotchiName}")`, function (err) {
      if (err) throw err;
      return res.status(200).json({success: true});
    });

  } catch (err) {
    res.status(500).json({
      success: false,
      error: `Error in Saving : "${err}`
    });
  }
};

/**
 * FIND ALL tamagotchi
 * @param {*} req
 * @param {*} res
 */
exports.findAll = (req, res) => {
  try {
    db.query(`SELECT *
              FROM tamagotchi`, function (err, result) {
      if (err) throw err;
      // No results
      if (result.length === 0) return res.json({success: true, data: 'No results'})
      // Found results
      return res.json({success: true, data: result})
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      error: `Error in get : "${err}`
    });
  }
};

/**
 * FIND tamagotchi BY ID
 * @param {*} req
 * @param {*} res
 */
exports.findById = (req, res) => {
  let params = req.params;
  if (!params.id || isNaN(params.id)) {
    res.status(500).json({
      success: false,
      error: "'id' required (Number)"
    });
    throw formatDate(Date.now(), "-", "long") + " 'id' required (Number)";
  }
  try {
    // Parsing string to int into try section just in case to catch error
    if (typeof params.id == "string") params.id = parseInt(params.id)

    db.query(`SELECT *
              FROM tamagotchi
              WHERE id = ${params.id}`, function (err, result) {
      if (err) throw err;
      // No results
      if (result.length === 0) return res.json({success: true, data: 'No results'})
      // Found results
      return res.json({success: true, data: result[0]})
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      error: `Error in get : "${err}`
    });
  }
};

/**
 * EXECUTE EAT ON TAMAGOTCHI
 * @param {*} req
 * @param {*} res
 */
exports.eat = (req, res) => {
  let params = req.body;
  if (!params.id || isNaN(params.id)) {
    res.status(500).json({
      success: false,
      error: "'id' required (Number)"
    });
    throw formatDate(Date.now(), "-", "long") + " 'id' required (Number)";
  }
  try {
    // Parsing string to int into try section just in case to catch error
    if (typeof params.id == "string") params.id = parseInt(params.id)

    db.query(`CALL EAT(${params.id})`, function (err) {
      if (err) throw err;
      // Found results
      return res.json({success: true})
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      error: `Error in eat method : "${err}`
    });
  }
};

/**
 * EXECUTE DRINK ON TAMAGOTCHI
 * @param {*} req
 * @param {*} res
 */
exports.drink = (req, res) => {
  let params = req.body;
  if (!params.id || isNaN(params.id)) {
    res.status(500).json({
      success: false,
      error: "'id' required (Number)"
    });
    throw formatDate(Date.now(), "-", "long") + " 'id' required (Number)";
  }
  try {
    // Parsing string to int into try section just in case to catch error
    if (typeof params.id == "string") params.id = parseInt(params.id)

    db.query(`CALL DRINK(${params.id})`, function (err) {
      if (err) throw err;
      // Found results
      return res.json({success: true})
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      error: `Error in eat method : "${err}`
    });
  }
};

/**
 * EXECUTE BETIME ON TAMAGOTCHI
 * @param {*} req
 * @param {*} res
 */
exports.bedtime = (req, res) => {
  let params = req.body;
  if (!params.id || isNaN(params.id)) {
    res.status(500).json({
      success: false,
      error: "'id' required (Number)"
    });
    throw formatDate(Date.now(), "-", "long") + " 'id' required (Number)";
  }
  try {
    // Parsing string to int into try section just in case to catch error
    if (typeof params.id == "string") params.id = parseInt(params.id)

    db.query(`CALL BEDTIME(${params.id})`, function (err) {
      if (err) throw err;
      // Found results
      return res.json({success: true})
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      error: `Error in eat method : "${err}`
    });
  }
};

/**
 * EXECUTE ENJOY ON TAMAGOTCHI
 * @param {*} req
 * @param {*} res
 */
exports.enjoy = (req, res) => {
  let params = req.body;
  if (!params.id || isNaN(params.id)) {
    res.status(500).json({
      success: false,
      error: "'id' required (Number)"
    });
    throw formatDate(Date.now(), "-", "long") + " 'id' required (Number)";
  }
  try {
    // Parsing string to int into try section just in case to catch error
    if (typeof params.id == "string") params.id = parseInt(params.id)

    db.query(`CALL ENJOY(${params.id})`, function (err) {
      if (err) throw err;
      // Found results
      return res.json({success: true})
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      error: `Error in eat method : "${err}`
    });
  }
};

/**
 * GET TAMAGOTCHIS iS ALIVE
 * @param {*} req
 * @param {*} res
 */
exports.alive = (req, res) => {
  let params = req.params;
  if (!params.id || isNaN(params.id)) {
    res.status(500).json({
      success: false,
      error: "'id' required (Number)"
    });
    throw formatDate(Date.now(), "-", "long") + " 'id' required (Number)";
  }
  try {
    // Parsing string to int into try section just in case to catch error
    if (typeof params.id == "string") params.id = parseInt(params.id)

    db.query(`
        select life, deaths.date_of_death as death, deaths.reason as reason
        from tamagotchi_life
                 left join deaths on deaths.tamagotchi_id = life -> "$.id"
        where life -> "$.id" = ${params.id}
          and life -> "$.is_alive" = 1`, function (err, result) {
      if (err) throw err;
      // No results
      if (result.length === 0) return res.json({success: true, data: 'No results'})
      // Found results
      return res.json({success: true, data: result[0]})
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      error: `Error in eat method : "${err}`
    });
  }
};

/**
 * GET TAMAGOTCHIS iS NOT ALIVE
 * @param {*} req
 * @param {*} res
 */
exports.dead = (req, res) => {
  let params = req.params;
  if (!params.id || isNaN(params.id)) {
    res.status(500).json({
      success: false,
      error: "'id' required (Number)"
    });
    throw formatDate(Date.now(), "-", "long") + " 'id' required (Number)";
  }
  try {
    // Parsing string to int into try section just in case to catch error
    if (typeof params.id == "string") params.id = parseInt(params.id)

    db.query(`
        select life
        from tamagotchi_life
        where life -> "$.id" = ${params.id}
          and life -> "$.is_alive" = 0`, function (err, result) {
      if (err) throw err;
      // No results
      if (result.length === 0) return res.json({success: true, data: 'No results'})
      // Found results
      return res.json({success: true, data: result[0]})
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      error: `Error in eat method : "${err}`
    });
  }
};
