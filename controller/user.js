const db = require('../config/Database');
const {formatDate} = require('../utils/helper')

/**
 * CREATE A USER AND ADD THIS FIRST tamagotchi
 * @param {*} req
 * @param {*} res
 */
exports.create = (req, res) => {

  let user = req.body

  if (!user.username || !user.tamagotchiName) {
    res.status(500).json({
      success: false,
      error: "'username','tamagotchiName' required (String)"
    });
    throw formatDate(Date.now(), "-", "long") + " 'username','tamagotchiName' required (String)";
  }
  // CALL CREATE_ACCOUNT procedure, it takes : username and tamagotchi name
  try {
    db.query(`CALL CREATE_ACCOUNT("${user.username}", "${user.tamagotchiName}")`, function (err) {
      if (err) throw err;
      return res.status(200).json({
        success: true,
      });
    });
  } catch (err) {
    res.status(500).send("Error in Saving : " + err);
  }
};

/**
 * GET USER THE GIVEN USERNAME EXIST
 * @param {*} req
 * @param {*} res
 */
 exports.getUser = (req, res) => {

  let params = req.body;

  if (!params.username) {
    res.status(500).json({
      success: false,
      error: "'username' required (String)"
    });
    throw formatDate(Date.now(), "-", "long") + " 'username' required (String)";
  }
  try {

    db.query(`
        SELECT *
        FROM user
        WHERE username = "${params.username}"`, function (err, result) {
      if (err) throw err;
      // No results
      if (result.length === 0) return res.json({success: true, data: 'No results'})
      // Found results
      return res.json({success: true, data: result[0]})
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      error: `Error in get : "${err}`
    });
  }
};

/**
 * FIND IF THE GIVEN USERNAME EXIST
 * @param {*} req
 * @param {*} res
 */
exports.isUsernameExist = (req, res) => {

  let params = req.body;

  if (!params.username) {
    res.status(500).json({
      success: false,
      error: "'username' required (String)"
    });
    throw formatDate(Date.now(), "-", "long") + " 'username' required (String)";
  }
  try {

    db.query(`
        SELECT *
        FROM user
        WHERE username = "${params.username}"`, function (err, result) {
      if (err) throw err;
      // No results
      if (result.length === 0) return res.json({success: true, data: false})
      // Found results
      return res.json({success: true, data: true})
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      error: `Error in get : "${err}`
    });
  }
};

/**
 * FIND ALL tamagotchi's USER WITH THIS ID
 * YUK NAME FUNCTION
 * @param {*} req
 * @param {*} res
 */
exports.findAllTamagotchiByUserId = (req, res) => {

  let params = req.params;

  if (!params.id || isNaN(params.id)) {
    res.status(500).json({
      success: false,
      error: "'id' required (Number)"
    });
    throw formatDate(Date.now(), "-", "long") + " 'id' required (Number)";
  }
  try {
    // Parsing string to int into try section just in case to catch error
    if (typeof params.id == "string") params.id = parseInt(params.id)

    db.query(`
        SELECT *
        FROM tamagotchi
        WHERE user_id = "${params.id}"`, function (err, result) {
      if (err) throw err;
      // No results
      if (result.length === 0) return res.json({success: true, data: 'No results'})
      // Found results
      return res.json({success: true, data: result})
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      error: `Error in get : "${err}`
    });
  }
};

/**
 * GET TAMAGOTCHIS iS ALIVE
 * @param {*} req
 * @param {*} res
 */
exports.findAllAliveTamagotchiByUserId = (req, res) => {
  let params = req.params;
  if (!params.id || isNaN(params.id)) {
    res.status(500).json({
      success: false,
      error: "'id' required (Number)"
    });
    throw formatDate(Date.now(), "-", "long") + " 'id' required (Number)";
  }
  try {
    // Parsing string to int into try section just in case to catch error
    if (typeof params.id == "string") params.id = parseInt(params.id)

    db.query(`
        select life
        from tamagotchi_life
        where life->"$.user_id" = ${params.id}
          and life->"$.is_alive" = 1`, function (err, result) {
      if (err) throw err;
      // No results
      if (result.length === 0) return res.json({success: true, data: []})
      // Found results
      return res.json({success: true, data: result})
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      error: `Error in eat method : "${err}`
    });
  }
};

/**
 * GET TAMAGOTCHIS iS NOT ALIVE
 * @param {*} req
 * @param {*} res
 */
exports.findAllDeadTamagotchiByUserId = (req, res) => {
  let params = req.params;
  if (!params.id || isNaN(params.id)) {
    res.status(500).json({
      success: false,
      error: "'id' required (Number)"
    });
    throw formatDate(Date.now(), "-", "long") + " 'id' required (Number)";
  }
  try {
    // Parsing string to int into try section just in case to catch error
    if (typeof params.id == "string") params.id = parseInt(params.id)

    db.query(`
        select life, deaths.date_of_death as death, deaths.reason as reason
        from tamagotchi_life
                 left join deaths on deaths.tamagotchi_id = life -> "$.id"
        where life->"$.user_id" = ${params.id}
          and life->"$.is_alive" = 0`, function (err, result) {
      if (err) throw err;
      // No results
      if (result.length === 0) return res.json({success: true, data: []})
      // Found results
      return res.json({success: true, data: result})
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      error: `Error in eat method : "${err}`
    });
  }
};
