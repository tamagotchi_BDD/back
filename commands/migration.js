const arguments = process.argv.slice(2);
const migrationTool = require('../utils/migrations')
if(arguments.length <= 0 ) {
  console.log(`Actions are : 'makemigration' or 'migrate''`)
  process.exit(1)
}
switch (arguments[0]) {
  case 'makemigration':
    migrationTool.makeMigration()
    break
  case 'migrate':
    migrationTool.migrate()
    break
  default:
    console.log(`Actions are : 'makemigration' or 'migrate''`)
    process.exit()
}

