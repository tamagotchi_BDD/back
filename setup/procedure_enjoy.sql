DELIMITER ;;
CREATE PROCEDURE ENJOY(IN in_tamagotchi_id INT)
BEGIN
  INSERT INTO actions(tamagotchi_id, name) VALUES(in_tamagotchi_id, "Play");
END ;;
DELIMITER ;