DELIMITER ;;
CREATE PROCEDURE CREATE_DEATH(IN in_tamagotchi_id INT, IN in_reason CHAR(64))
BEGIN
  INSERT INTO deaths(tamagotchi_id, reason, date_of_death) VALUES(in_tamagotchi_id, in_reason, NOW());
END ;;
DELIMITER ;