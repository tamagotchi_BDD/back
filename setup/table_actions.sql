CREATE TABLE actions (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    tamagotchi_id INT NOT NULL,
    name VARCHAR(50) NOT NULL,
    FOREIGN KEY (tamagotchi_id) REFERENCES tamagotchi(id)
);