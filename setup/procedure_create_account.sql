DELIMITER ;;
CREATE PROCEDURE CREATE_ACCOUNT(IN in_username CHAR(64), IN in_tamagotchi_name CHAR(64))
BEGIN
  DECLARE temp_user_id CHAR(25);
  INSERT INTO user (username) VALUES(in_username);
  SELECT LAST_INSERT_ID() INTO temp_user_id;
  INSERT INTO tamagotchi (name, user_id) VALUES(in_tamagotchi_name, temp_user_id);
END ;;
DELIMITER ;
