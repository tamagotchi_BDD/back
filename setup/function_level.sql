DELIMITER ;;
CREATE FUNCTION LEVEL(in_tamagotchi_id INT) 
RETURNS INT DETERMINISTIC 
BEGIN 
    declare t_level INT DEFAULT NULL;
    select levels into t_level from tamagotchi where id = in_tamagotchi_id;
    IF (t_level IS NOT NULL) THEN 
        RETURN t_level;
    ELSE
        RETURN 0;
    END IF;
END ;;
DELIMITER ;
