DELIMITER ;;
CREATE PROCEDURE CREATE_ACTION(IN in_tamagotchi_id INT, IN in_name CHAR(64))
BEGIN
  INSERT INTO actions(tamagotchi_id, name) VALUES(in_tamagotchi_id, in_name);
END ;;
DELIMITER ;