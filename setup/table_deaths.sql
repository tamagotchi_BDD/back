CREATE TABLE deaths (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    tamagotchi_id INT NOT NULL,
    reason VARCHAR(50) NOT NULL,
    date_of_death DATETIME DEFAULT NULL,
    FOREIGN KEY (tamagotchi_id) REFERENCES tamagotchi(id)
);