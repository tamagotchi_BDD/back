DELIMITER ;;
CREATE TRIGGER SAVE_DEATH AFTER UPDATE ON tamagotchi 
FOR EACH ROW 
BEGIN 
  DECLARE temp_eat INT;
  DECLARE temp_drink INT;
  DECLARE temp_bedtime INT;
  DECLARE temp_enjoy INT;
  SELECT eat, drink, bedtime, enjoy INTO temp_eat, temp_drink, temp_bedtime, temp_enjoy FROM tamagotchi WHERE id=NEW.id;
  IF temp_eat = 0 THEN 
    CALL CREATE_DEATH(NEW.id, "I was hungry !"); 
  ELSEIF temp_drink = 0 THEN 
    CALL CREATE_DEATH(NEW.id, "I was thirsty !"); 
  ELSEIF temp_bedtime = 0 THEN 
    CALL CREATE_DEATH(NEW.id, "I was sleepy !"); 
  ELSEIF temp_enjoy = 0 THEN 
    CALL CREATE_DEATH(NEW.id, "I was weary !"); 
  END IF;
END ;;
DELIMITER ;