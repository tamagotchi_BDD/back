DELIMITER ;;
CREATE FUNCTION COMPUTE_STATS(in_value INT, in_given_value INT, in_sign CHAR(5), in_current_level INT) 
RETURNS INT DETERMINISTIC 
BEGIN
  DECLARE result INT DEFAULT 0;
  IF in_sign = "+" THEN
  SET result = in_value + (in_given_value + in_current_level - 1);
  ELSE
  SET result = in_value - (in_given_value + in_current_level - 1);
  END IF;
  IF result > 100 THEN 
  SET result = 100; 
  ELSEIF result <= 0 THEN
  SET result = 0;
  END IF;
  RETURN result;
END ;;
DELIMITER ;