DELIMITER ;;
CREATE PROCEDURE CREATE_TAMAGOTCHI(IN in_id_of_user INT, IN in_tamagotchi_name CHAR(64))
BEGIN
    INSERT INTO tamagotchi (name, user_id) VALUES(in_tamagotchi_name, in_id_of_user);
END ;;
DELIMITER ;