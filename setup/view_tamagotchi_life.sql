CREATE VIEW tamagotchi_life AS SELECT JSON_OBJECT(
  'id', t.id,
  'eat', t.eat,
  'enjoy', t.enjoy,
  'bedtime', t.bedtime,
  'drink', t.drink,
  'is_alive', IS_ALIVE(t.id),
  'user_id', t.user_id,
  'name', t.name,
  'levels', t.levels,
  'birth', t.date_of_birth
) AS life
 FROM tamagotchi as t;
