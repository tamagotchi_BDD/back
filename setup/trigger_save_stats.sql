DELIMITER ;;
CREATE TRIGGER SAVE_STATS AFTER INSERT ON actions 
FOR EACH ROW 
BEGIN
  DECLARE temp_eat INT;
  DECLARE temp_drink INT;
  DECLARE temp_bedtime INT;
  DECLARE temp_enjoy INT;
  DECLARE temp_level INT;
  DECLARE temp_xp INT;
  DECLARE temp_alive INT;
  SELECT eat, drink, bedtime, enjoy, levels, xps INTO temp_eat, temp_drink, temp_bedtime, temp_enjoy, temp_level, temp_xp FROM tamagotchi WHERE id=NEW.tamagotchi_id;
  SELECT IS_ALIVE(NEW.tamagotchi_id) AS temp_alive INTO temp_alive;
  IF temp_alive THEN 
    IF NEW.name = "Eat" AND temp_eat <= 80 THEN 
    SELECT COMPUTE_STATS(temp_eat, 30, "+", temp_level) AS temp_eat INTO temp_eat; 
    SELECT COMPUTE_STATS(temp_drink, 10, "-", temp_level) AS temp_drink INTO temp_drink; 
    SELECT COMPUTE_STATS(temp_bedtime, 5, "-", temp_level) AS temp_bedtime INTO temp_bedtime; 
    SELECT COMPUTE_STATS(temp_enjoy, 5, "-", temp_level) AS temp_enjoy INTO temp_enjoy;
    SET temp_xp = temp_xp + 1;
    ELSEIF NEW.name = "Drink" AND temp_drink <= 80 THEN 
    SELECT COMPUTE_STATS(temp_drink, 30, "+", temp_level) AS temp_drink INTO temp_drink; 
    SELECT COMPUTE_STATS(temp_eat, 10, "-", temp_level) AS temp_eat INTO temp_eat; 
    SELECT COMPUTE_STATS(temp_bedtime, 5, "-", temp_level) AS temp_bedtime INTO temp_bedtime; 
    SELECT COMPUTE_STATS(temp_enjoy, 5, "-", temp_level) AS temp_enjoy INTO temp_enjoy;
    SET temp_xp = temp_xp + 1;
    ELSEIF NEW.name = "Sleep" AND temp_bedtime <= 80 THEN 
    SELECT COMPUTE_STATS(temp_bedtime, 30, "+", temp_level) AS temp_bedtime INTO temp_bedtime; 
    SELECT COMPUTE_STATS(temp_eat, 10, "-", temp_level) AS temp_eat INTO temp_eat; 
    SELECT COMPUTE_STATS(temp_drink, 15, "-", temp_level) AS temp_drink INTO temp_drink; 
    SELECT COMPUTE_STATS(temp_enjoy, 15, "-", temp_level) AS temp_enjoy INTO temp_enjoy;
    SET temp_xp = temp_xp + 1;
    ELSEIF NEW.name = "Play" AND temp_enjoy <= 80 THEN 
    SELECT COMPUTE_STATS(temp_enjoy, 15, "+", temp_level) AS temp_enjoy INTO temp_enjoy; 
    SELECT COMPUTE_STATS(temp_bedtime, 5, "-", temp_level) AS temp_bedtime INTO temp_bedtime;
    SELECT COMPUTE_STATS(temp_eat, 5, "-", temp_level) AS temp_eat INTO temp_eat; 
    SELECT COMPUTE_STATS(temp_drink, 5, "-", temp_level) AS temp_drink INTO temp_drink; 
    SET temp_xp = temp_xp + 1;
    END IF; 
  SET temp_level = 1 + floor(temp_xp/10);
  UPDATE tamagotchi SET eat=temp_eat, drink=temp_drink, bedtime=temp_bedtime, enjoy=temp_enjoy, xps=temp_xp, levels=temp_level WHERE id=NEW.tamagotchi_id;
  END IF; 
END ;;
DELIMITER ;