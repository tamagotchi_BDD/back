DELIMITER ;;
CREATE PROCEDURE CREATE_MIGRATION(IN in_current CHAR(64), IN in_old CHAR(64))
BEGIN
  INSERT INTO migrations(current, old) VALUES(in_current, in_old);
END ;;
DELIMITER ;