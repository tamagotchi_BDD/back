DELIMITER ;;
CREATE PROCEDURE EAT(IN in_tamagotchi_id INT)
BEGIN
  INSERT INTO actions(tamagotchi_id, name) VALUES(in_tamagotchi_id, "Eat");
END ;;
DELIMITER ;