DELIMITER ;;
CREATE PROCEDURE DRINK(IN in_tamagotchi_id INT)
BEGIN
  INSERT INTO actions(tamagotchi_id, name) VALUES(in_tamagotchi_id, "Drink");
END ;;
DELIMITER ;