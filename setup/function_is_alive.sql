DELIMITER ;;
CREATE FUNCTION IS_ALIVE(in_tamagotchi_id INT) 
RETURNS BOOL DETERMINISTIC 
BEGIN
    declare tamagotchi INT DEFAULT NULL;
    select tamagotchi_id into tamagotchi from deaths where tamagotchi_id = in_tamagotchi_id;
    IF (tamagotchi IS NOT NULL) THEN
        RETURN 0;
    ELSE
        RETURN 1;
    END IF;
END ;;
DELIMITER ;
