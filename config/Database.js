const mysql = require('mysql2');
const path = require('path');
require('dotenv').config({path: path.resolve(__dirname, '../.env')});

class Database {
    constructor() {
        this.host =     process.env.DB_HOST;
        this.user =     process.env.DB_USER;
        this.password = process.env.DB_PASSWORD;
        this.database = process.env.DB_NAME;
    }

    instance() {
        return mysql.createConnection({
            host: this.host,
            user: this.user,
            password: this.password,
            database: this.database
        });
    }
}

let database = new Database();

database
    .instance()
    .connect(error => {
        if (error) console.log('Erreur lors de la connexion à la base de donnée. Elle n\'existe pas.')
        else console.log("Successfully connected to the database.");
    });

module.exports = database.instance();
