const db = require('../config/Database');

exports.createTransaction = (array) => {
  return db
    .promise()
    .connect()
    .then(() => {
      db.query('START TRANSACTION');
    })
    .then(() => {
      array.forEach((query) => {
        db.query(query);
      })
    })
    .then(() => {
      db.query('COMMIT');
    })
    .catch(() => {
      console.log('testbug');
      db.query('ROLLBACK');
    })
    ;
};

exports.executeQuery = async (query) => {
  return await db.promise().query(query);
};
