const express = require("express");
const app = express();
const cors = require('cors')
require('dotenv').config()
const port = process.env.APP_PORT;


app.use(express.json());
app.use(cors({ origin: [`${process.env.REACT_APP_PROTOCOL}://${process.env.REACT_APP_HOST}:${process.env.REACT_APP_PORT}`], credentials: true }));

app.use(express.urlencoded({extended: true,}));

// Import router
require("./routes/index.js")(app)

app.listen(port, () => {console.log(`Server is running on port ${port}.`)});