-- MySQL dump 10.13  Distrib 5.7.31, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: bddav
-- ------------------------------------------------------
-- Server version	5.7.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actions`
--

DROP TABLE IF EXISTS `actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tamagotchi_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tamagotchi_id` (`tamagotchi_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actions`
--

LOCK TABLES `actions` WRITE;
/*!40000 ALTER TABLE `actions` DISABLE KEYS */;
INSERT INTO `actions` (`id`, `tamagotchi_id`, `name`) VALUES (1,1,'Eat'),(2,2,'Drink'),(3,3,'Sleep'),(4,4,'Play');
/*!40000 ALTER TABLE `actions` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO' */ ;
/*!50032 DROP TRIGGER IF EXISTS SAVE_STATS */;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER SAVE_STATS AFTER INSERT ON actions FOR EACH ROW BEGIN  DECLARE temp_eat INT;  DECLARE temp_drink INT;  DECLARE temp_bedtime INT;  DECLARE temp_enjoy INT;  DECLARE temp_level INT;  DECLARE temp_xp INT;  DECLARE temp_alive INT;  SELECT eat, drink, bedtime, enjoy, levels, xps INTO temp_eat, temp_drink, temp_bedtime, temp_enjoy, temp_level, temp_xp FROM tamagotchi WHERE id=NEW.tamagotchi_id;  SELECT IS_ALIVE(NEW.tamagotchi_id) AS temp_alive INTO temp_alive;  IF temp_alive THEN     IF NEW.name = "Eat" AND temp_eat <= 80 THEN     SELECT COMPUTE_STATS(temp_eat, 30, "+", temp_level) AS temp_eat INTO temp_eat;     SELECT COMPUTE_STATS(temp_drink, 10, "-", temp_level) AS temp_drink INTO temp_drink;     SELECT COMPUTE_STATS(temp_bedtime, 5, "-", temp_level) AS temp_bedtime INTO temp_bedtime;     SELECT COMPUTE_STATS(temp_enjoy, 5, "-", temp_level) AS temp_enjoy INTO temp_enjoy;    SET temp_xp = temp_xp + 1;    ELSEIF NEW.name = "Drink" AND temp_drink <= 80 THEN     SELECT COMPUTE_STATS(temp_drink, 30, "+", temp_level) AS temp_drink INTO temp_drink;     SELECT COMPUTE_STATS(temp_eat, 10, "-", temp_level) AS temp_eat INTO temp_eat;     SELECT COMPUTE_STATS(temp_bedtime, 5, "-", temp_level) AS temp_bedtime INTO temp_bedtime;     SELECT COMPUTE_STATS(temp_enjoy, 5, "-", temp_level) AS temp_enjoy INTO temp_enjoy;    SET temp_xp = temp_xp + 1;    ELSEIF NEW.name = "Sleep" AND temp_bedtime <= 80 THEN     SELECT COMPUTE_STATS(temp_bedtime, 30, "+", temp_level) AS temp_bedtime INTO temp_bedtime;     SELECT COMPUTE_STATS(temp_eat, 10, "-", temp_level) AS temp_eat INTO temp_eat;     SELECT COMPUTE_STATS(temp_drink, 15, "-", temp_level) AS temp_drink INTO temp_drink;     SELECT COMPUTE_STATS(temp_enjoy, 15, "-", temp_level) AS temp_enjoy INTO temp_enjoy;    SET temp_xp = temp_xp + 1;    ELSEIF NEW.name = "Play" AND temp_enjoy <= 80 THEN     SELECT COMPUTE_STATS(temp_enjoy, 15, "+", temp_level) AS temp_enjoy INTO temp_enjoy;     SELECT COMPUTE_STATS(temp_bedtime, 5, "-", temp_level) AS temp_bedtime INTO temp_bedtime;    SELECT COMPUTE_STATS(temp_eat, 5, "-", temp_level) AS temp_eat INTO temp_eat;     SELECT COMPUTE_STATS(temp_drink, 5, "-", temp_level) AS temp_drink INTO temp_drink;     SET temp_xp = temp_xp + 1;    END IF;   SET temp_level = 1 + floor(temp_xp/10);  UPDATE tamagotchi SET eat=temp_eat, drink=temp_drink, bedtime=temp_bedtime, enjoy=temp_enjoy, xps=temp_xp, levels=temp_level WHERE id=NEW.tamagotchi_id;  END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `deaths`
--

DROP TABLE IF EXISTS `deaths`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deaths` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tamagotchi_id` int(11) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `date_of_death` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tamagotchi_id` (`tamagotchi_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deaths`
--

LOCK TABLES `deaths` WRITE;
/*!40000 ALTER TABLE `deaths` DISABLE KEYS */;
/*!40000 ALTER TABLE `deaths` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `current` varchar(50) NOT NULL,
  `old` varchar(50) NOT NULL,
  `date_of_creation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `current`, `old`, `date_of_creation`) VALUES (1,'migration99999999.sql','','2022-11-13 14:24:42');
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tamagotchi`
--

DROP TABLE IF EXISTS `tamagotchi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tamagotchi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT 'Riri',
  `levels` int(11) NOT NULL DEFAULT '1',
  `xps` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `eat` int(11) NOT NULL DEFAULT '70',
  `drink` int(11) NOT NULL DEFAULT '70',
  `bedtime` int(11) NOT NULL DEFAULT '70',
  `enjoy` int(11) NOT NULL DEFAULT '70',
  `date_of_birth` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tamagotchi`
--

LOCK TABLES `tamagotchi` WRITE;
/*!40000 ALTER TABLE `tamagotchi` DISABLE KEYS */;
INSERT INTO `tamagotchi` (`id`, `name`, `levels`, `xps`, `user_id`, `eat`, `drink`, `bedtime`, `enjoy`, `date_of_birth`) VALUES (1,'Jasmin',1,1,1,100,60,65,65,'2022-11-13 14:24:42'),(2,'Phantom',1,1,1,60,100,65,65,'2022-11-13 14:24:42'),(3,'Shelby',1,1,1,60,55,100,55,'2022-11-13 14:24:42'),(4,'Hyuujin',1,1,1,65,65,65,85,'2022-11-13 14:24:42');
/*!40000 ALTER TABLE `tamagotchi` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO' */ ;
/*!50032 DROP TRIGGER IF EXISTS SAVE_DEATH */;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER SAVE_DEATH AFTER UPDATE ON tamagotchi FOR EACH ROW BEGIN   DECLARE temp_eat INT;  DECLARE temp_drink INT;  DECLARE temp_bedtime INT;  DECLARE temp_enjoy INT;  SELECT eat, drink, bedtime, enjoy INTO temp_eat, temp_drink, temp_bedtime, temp_enjoy FROM tamagotchi WHERE id=NEW.id;  IF temp_eat = 0 THEN     CALL CREATE_DEATH(NEW.id, "I was hungry !");   ELSEIF temp_drink = 0 THEN     CALL CREATE_DEATH(NEW.id, "I was thirsty !");   ELSEIF temp_bedtime = 0 THEN     CALL CREATE_DEATH(NEW.id, "I was sleepy !");   ELSEIF temp_enjoy = 0 THEN     CALL CREATE_DEATH(NEW.id, "I was weary !");   END IF;END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Temporary table structure for view `tamagotchi_life`
--

DROP TABLE IF EXISTS `tamagotchi_life`;
/*!50001 DROP VIEW IF EXISTS `tamagotchi_life`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `tamagotchi_life` AS SELECT 
 1 AS `life`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`) VALUES (1,'Léana');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'bddav'
--

--
-- Dumping routines for database 'bddav'
--
/*!50003 DROP FUNCTION IF EXISTS `COMPUTE_STATS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `COMPUTE_STATS`(in_value INT, in_given_value INT, in_sign CHAR(5), in_current_level INT) RETURNS int(11)
    DETERMINISTIC
BEGIN  DECLARE result INT DEFAULT 0;  IF in_sign = "+" THEN  SET result = in_value + (in_given_value + in_current_level - 1);  ELSE  SET result = in_value - (in_given_value + in_current_level - 1);  END IF;  IF result > 100 THEN   SET result = 100;   ELSEIF result <= 0 THEN  SET result = 0;  END IF;  RETURN result;END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `IS_ALIVE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `IS_ALIVE`(in_tamagotchi_id INT) RETURNS tinyint(1)
    DETERMINISTIC
BEGIN    declare tamagotchi INT DEFAULT NULL;    select tamagotchi_id into tamagotchi from deaths where tamagotchi_id = in_tamagotchi_id;    IF (tamagotchi IS NOT NULL) THEN        RETURN 0;    ELSE        RETURN 1;    END IF;END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `LEVEL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `LEVEL`(in_tamagotchi_id INT) RETURNS int(11)
    DETERMINISTIC
BEGIN     declare t_level INT DEFAULT NULL;    select levels into t_level from tamagotchi where id = in_tamagotchi_id;    IF (t_level IS NOT NULL) THEN         RETURN t_level;    ELSE        RETURN 0;    END IF;END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `BEDTIME` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `BEDTIME`(IN in_tamagotchi_id INT)
BEGIN  INSERT INTO actions(tamagotchi_id, name) VALUES(in_tamagotchi_id, "Sleep");END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `CREATE_ACCOUNT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `CREATE_ACCOUNT`(IN in_username CHAR(64), IN in_tamagotchi_name CHAR(64))
BEGIN  DECLARE temp_user_id CHAR(25);  INSERT INTO user (username) VALUES(in_username);  SELECT LAST_INSERT_ID() INTO temp_user_id;  INSERT INTO tamagotchi (name, user_id) VALUES(in_tamagotchi_name, temp_user_id);END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `CREATE_ACTION` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `CREATE_ACTION`(IN in_tamagotchi_id INT, IN in_name CHAR(64))
BEGIN  INSERT INTO actions(tamagotchi_id, name) VALUES(in_tamagotchi_id, in_name);END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `CREATE_DEATH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `CREATE_DEATH`(IN in_tamagotchi_id INT, IN in_reason CHAR(64))
BEGIN  INSERT INTO deaths(tamagotchi_id, reason, date_of_death) VALUES(in_tamagotchi_id, in_reason, NOW());END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `CREATE_MIGRATION` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `CREATE_MIGRATION`(IN in_current CHAR(64), IN in_old CHAR(64))
BEGIN  INSERT INTO migrations(current, old) VALUES(in_current, in_old);END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `CREATE_TAMAGOTCHI` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `CREATE_TAMAGOTCHI`(IN in_id_of_user INT, IN in_tamagotchi_name CHAR(64))
BEGIN    INSERT INTO tamagotchi (name, user_id) VALUES(in_tamagotchi_name, in_id_of_user);END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DRINK` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `DRINK`(IN in_tamagotchi_id INT)
BEGIN  INSERT INTO actions(tamagotchi_id, name) VALUES(in_tamagotchi_id, "Drink");END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EAT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `EAT`(IN in_tamagotchi_id INT)
BEGIN  INSERT INTO actions(tamagotchi_id, name) VALUES(in_tamagotchi_id, "Eat");END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ENJOY` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ENJOY`(IN in_tamagotchi_id INT)
BEGIN  INSERT INTO actions(tamagotchi_id, name) VALUES(in_tamagotchi_id, "Play");END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `tamagotchi_life`
--

/*!50001 DROP VIEW IF EXISTS `tamagotchi_life`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `tamagotchi_life` AS select json_object('id',`t`.`id`,'eat',`t`.`eat`,'enjoy',`t`.`enjoy`,'bedtime',`t`.`bedtime`,'drink',`t`.`drink`,'is_alive',`IS_ALIVE`(`t`.`id`),'user_id',`t`.`user_id`,'name',`t`.`name`,'levels',`t`.`levels`,'birth',`t`.`date_of_birth`) AS `life` from `tamagotchi` `t` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-13 14:24:57
