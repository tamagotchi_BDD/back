const tamagotchiController = require('../controller/tamagotchi');
let router = require("express").Router();

const endpoint = "/tamagotchi"

router.get(endpoint + '/', tamagotchiController.findAll);
router.get(endpoint + '/:id', tamagotchiController.findById);
router.post(endpoint + '/', tamagotchiController.create);
router.post(endpoint + '/eat/', tamagotchiController.eat);
router.post(endpoint + '/drink/', tamagotchiController.drink);
router.post(endpoint + '/bedtime/', tamagotchiController.bedtime);
router.post(endpoint + '/enjoy/', tamagotchiController.enjoy);
router.get(endpoint + '/alive/:id', tamagotchiController.alive);
router.get(endpoint + '/dead/:id', tamagotchiController.dead);

module.exports = router;
