const fs = require("fs");
const path = require("path");

module.exports = (app) => {

  fs.readdir(path.resolve(__dirname) + '/', (err, files) => {
      // import all file inside routes folder except index.js
      let routeFiles = files.filter(file => file !== 'index.js')
      routeFiles.forEach(file => {
        app.use('/api', require('./' + file));
    });
  })
};
