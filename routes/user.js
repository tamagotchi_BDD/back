const userController = require('../controller/user');
let router = require("express").Router();

const endpoint = "/user"

router.get(endpoint + '/tamagotchi/:id', userController.findAllTamagotchiByUserId);
router.get(endpoint + '/tamagotchi/alive/:id', userController.findAllAliveTamagotchiByUserId);
router.get(endpoint + '/tamagotchi/dead/:id', userController.findAllDeadTamagotchiByUserId);
router.post(endpoint + '/', userController.create);
router.post(endpoint + '/search/', userController.getUser);
router.post(endpoint + '/exist/', userController.isUsernameExist);

module.exports = router;
